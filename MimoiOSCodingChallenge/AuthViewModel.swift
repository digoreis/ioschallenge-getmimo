//
//  AuthViewModel.swift
//  MimoiOSCodingChallenge
//
//  Created by Rodrigo Reis on 07/04/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import Foundation

class AuthResponse : NSObject {
    var status: Bool
    var user : UserResponse?
    var error : String?
    
    init(status: Bool, user: UserResponse?, error: String?) {
        self.status = status
        self.user = user
        self.error = error
    }
}

class UserResponse : NSObject {
    var email: String = ""
    let token: String
    let accessToken: String
    init(token: String, accessToken: String) {
       self.token = token
       self.accessToken = accessToken
    }
}

protocol AuthFeedback {
    func autenticate(response: AuthResponse)
    func create(response: AuthResponse)
    func logout(response: AuthResponse)
}

class AuthViewModel : NSObject {
    
    var response : AuthResponse?
    
    open func autenticate(userName: String, password: String,feedback:AuthFeedback) {
        AuthServices.login(userName: userName, password: password){ response in
            self.response = response
            feedback.autenticate(response: response)
        }
    }
    
    open func create(userName: String, password: String,feedback:AuthFeedback) {
        AuthServices.create(userName: userName, password: password){ response in
            self.response = response
            feedback.create(response: response)
        }
        
    }
    
    func logout(feedback:@escaping (Bool) -> Void) {
        AuthServices.logout { result in
            if result.status {
                self.response = nil
            }
            feedback(result.status)
        }
    }
    
    open func saveData(key : String,value: Bool) {
        
    }
    
    open func getData(key: String) -> Bool {
        return false
    }
}
