//
//  AuthViewController.swift
//  MimoiOSCodingChallenge
//
//  Created by Rodrigo Reis on 07/04/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController, AuthFeedback {
    
    var userField : UITextField!
    var passwordField : UITextField!
    var statusLabel : UILabel!
    
    let viewModel = AuthViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
    }
    
    public func login(_ sender:Any) {
        viewModel.autenticate(userName: userField.text ?? "", password: passwordField.text ?? "", feedback: self)
    }
    
    public func signup(_ sender:Any) {
        viewModel.create(userName: userField.text ?? "", password: passwordField.text ?? "", feedback: self)
    }
    
    func showAlert(text: String) {
        let alert = UIAlertController(title: "Alert", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

}

extension AuthViewController {
    func autenticate(response: AuthResponse) {
        if response.status  {
            DispatchQueue.main.async {
                let vc = SettingsViewController()
                vc.setAuthModel(self.viewModel)
                self.present(vc, animated: true, completion: nil)
            }
        } else if let error = response.error {
            self.showAlert(text: error)
        }
    }
    func create(response: AuthResponse) {
        if response.status  {
            DispatchQueue.main.async {
                let vc = SettingsViewController()
                vc.setAuthModel(self.viewModel)
                self.present(vc, animated: true, completion: nil)
            }
        } else if let error = response.error {
            self.showAlert(text: error)
        }
    }
    func logout(response: AuthResponse) {
        
    }
}

extension AuthViewController {
    
    fileprivate func setupInterface() {
        view = UIView(frame: CGRect.zero)
        view.backgroundColor = .white
        let baseStackView = UIStackView()
        baseStackView.axis = .vertical
        baseStackView.distribution = .equalSpacing
        baseStackView.alignment = .center
        baseStackView.spacing = 10
        baseStackView.addArrangedSubview(header())
        baseStackView.addArrangedSubview(body())
        baseStackView.addArrangedSubview(footer())
        baseStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(baseStackView)
        baseStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        baseStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    fileprivate func header() -> UIView {
        let header = UIView()
        header.heightAnchor.constraint(equalToConstant: 100).isActive = true
        header.widthAnchor.constraint(equalToConstant: 300).isActive = true
        return header
    }
    
    fileprivate func body() -> UIView {
        let body = UIView()
        body.heightAnchor.constraint(equalToConstant: 300).isActive = true
        body.widthAnchor.constraint(equalToConstant: 300).isActive = true
        userForm(view: body)
        return body
    }
    
    fileprivate func userForm(view : UIView) {
        let formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.distribution = .equalSpacing
        formStackView.alignment = .center
        formStackView.spacing = 10
        userField = inputText(placeholder: "Login", isPassword: false)
        passwordField = inputText(placeholder: "Password", isPassword: true)
        formStackView.addArrangedSubview(userField)
        formStackView.addArrangedSubview(passwordField)
        formStackView.addArrangedSubview(actionButton(title: "Login", action: #selector(login(_:))))
        formStackView.addArrangedSubview(actionButton(title: "Sign up", action: #selector(signup(_:))))
        formStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(formStackView)
        formStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        formStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    fileprivate func inputText(placeholder: String, isPassword: Bool) -> UITextField {
        let text = UITextField()
        text.placeholder = placeholder
        text.isSecureTextEntry = isPassword
        text.heightAnchor.constraint(equalToConstant: 30).isActive = true
        text.widthAnchor.constraint(equalToConstant: 200).isActive = true
        return text
    }
    
    fileprivate func actionButton(title: String, action: Selector) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        return button
    }
    
    fileprivate func labelText(defaultText: String) -> UILabel {
        let label = UILabel()
        label.text = defaultText
        label.textColor = .darkGray
        label.textAlignment = .center
        label.numberOfLines = 3
        label.heightAnchor.constraint(equalToConstant: 90).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
        return label
    }
    
    fileprivate func footer() -> UIView {
        let footer = UIView()
        
        footer.heightAnchor.constraint(equalToConstant: 100).isActive = true
        footer.widthAnchor.constraint(equalToConstant: 300).isActive = true
        statusLabel = labelText(defaultText: "")
        footer.addSubview(statusLabel)
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        statusLabel.centerXAnchor.constraint(equalTo: footer.centerXAnchor).isActive = true
        statusLabel.centerYAnchor.constraint(equalTo: footer.centerYAnchor).isActive = true
        return footer
    }
}
