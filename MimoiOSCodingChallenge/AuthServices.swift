//
//  AuthServices.swift
//  MimoiOSCodingChallenge
//
//  Created by Rodrigo Reis on 07/04/17.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import Foundation

class AuthServices {
    
    fileprivate static let baseURL = "https://mimo-test.auth0.com"
    fileprivate static let clientID = "PAn11swGbMAVXVDbSCpnITx5Utsxz1co"
    
    static func login(userName: String,password : String, callback: @escaping (AuthResponse) -> Void) {
        
        var request = URLRequest(url: URL(string: "\(baseURL)/oauth/ro")!)
        request.httpMethod = "POST"
        let postString = LoginBody(client_id: clientID, username: userName, password: password).toJSON()
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let result = AuthResponse(status: false, user: nil, error: nil)
            guard let data = data, error == nil else {
                callback(result)
                return
            }
            
    
            
            do {
                
                let parsedData = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    result.error = parsedData["error_description"] as? String
                    callback(result)
                    return
                }
                
                if let token = parsedData["id_token"] as? String,let access = parsedData["access_token"] as? String {
                    result.user = UserResponse(token: token, accessToken: access)
                    self.userInfo(accessToken: access, response: result, callback: { response in
                        callback(response)
                    })
                }
                
            } catch let error as NSError {
                result.error = error.localizedDescription
                callback(result)
            }
            
        }
        task.resume()
    }
    
    static func logout(callback: @escaping (AuthResponse) -> Void) {
        
        var request = URLRequest(url: URL(string: "\(baseURL)/v2/logout")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let result = AuthResponse(status: false, user: nil, error: nil)
            guard error == nil else {
                callback(result)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                result.error = error?.localizedDescription
                callback(result)
                return
            }
            result.user = UserResponse(token: "", accessToken: "")
            result.status = true
            callback(result)
        }
        task.resume()
    }
    
    static func create(userName: String,password : String, callback: @escaping (AuthResponse) -> Void) {
        
        var request = URLRequest(url: URL(string: "\(baseURL)/dbconnections/signup")!)
        request.httpMethod = "POST"
        let postString = CreateAuthBody(client_id: clientID, username: userName, password: password).toJSON()
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        let task =  URLSession.shared.dataTask(with: request) { data, response, error in
            let result = AuthResponse(status: false, user: nil, error: nil)
            guard let data = data, error == nil else {
                callback(result)
                return
            }
            
            
            let parsedData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                result.error = parsedData?["description"] as? String
                callback(result)
                return
            }
            
            if let token = parsedData?["id_token"] as? String,let access = parsedData?["access_token"] as? String {
                result.user = UserResponse(token: token, accessToken: access)
                self.userInfo(accessToken: token, response: result, callback: { response in
                    callback(response)
                })
            }
        }
        task.resume()
    }
    
    static func userInfo(accessToken: String, response: AuthResponse, callback: @escaping (AuthResponse) -> Void) {
        
        let response = response
        var request = URLRequest(url: URL(string: "\(baseURL)/userinfo")!)
        request.httpMethod = "POST"
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "authorization")
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        let task = URLSession.shared.dataTask(with: request) { data, responseHttp, error in
            guard let data = data, error == nil else {
                response.error = error?.localizedDescription
                callback(response)
                return
            }
            
            if let httpStatus = responseHttp as? HTTPURLResponse, httpStatus.statusCode != 200 {
                response.error = error?.localizedDescription
                callback(response)
            }
            
            let parsedData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            if let email = parsedData?["email"] as? String {
                response.user?.email = email
            }
            response.status = true
            callback(response)
            
        }
        task.resume()
    }
    
}

extension AuthServices {
    struct LoginBody {
        let client_id : String
        let username : String
        let password : String
        let connection = "Username-Password-Authentication"
        let scope = "openid profile email"
        let grant_type = "password"
        
        func toJSON() -> String {
            return "{ \"client_id\": \"\(client_id)\",\"username\": \"\(username)\",\"password\": \"\(password)\",\"connection\": \"\(connection)\",\"scope\": \"\(scope)\",\"grant_type\": \"\(grant_type)\"}"
        }
    }
    
    struct CreateAuthBody {
        let client_id : String
        let username : String
        let password : String
        let connection = "Username-Password-Authentication"
        
        func toJSON() -> String {
            return "{ \"client_id\": \"\(client_id)\",\"email\": \"\(username)\",\"password\": \"\(password)\",\"connection\": \"\(connection)\"}"
        }
    }
}



